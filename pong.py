# -*- coding: utf-8 -*-
import os
os.environ["PYSDL2_DLL_PATH"] = os.path.join(os.getcwd(), "libs")
import sys
from sdl2 import *
from sdl2.sdlimage import *
from sdl2.sdlmixer import *
import sdl2.ext as ext
from sdl2.ext.compat import byteify
from input import *
from random import randint


# Work frozen, or raw.
if getattr(sys, 'frozen', False):
    RESOURCES = ext.Resources(sys.executable, "resources")
else:
    RESOURCES = ext.Resources(__file__, "resources")
TARGETFPS = 60
WHITE = ext.Color(255, 255, 255)
BLACK = ext.Color(0, 0, 0)
TURQUOISE = ext.Color(5, 55, 55)
LIGHT_BLUE = ext.Color(0, 153, 255)
BLUE = ext.Color(0, 0, 255)

screen_width = 960
screen_height = 540


class AudioSystem(ext.System):
    def __init__(self, frequency=44100, channels=2, buffer_bytes=4096):
        super(AudioSystem, self).__init__()
        self.componenttypes = (SoundEffects, PlayerData)
        Mix_OpenAudio(frequency, MIX_DEFAULT_FORMAT, channels, buffer_bytes)

        music_file = RESOURCES.get_path("POL-sad-story.wav")
        background_music = Mix_LoadMUS(byteify(music_file, "utf-8"))
        #Mix_PlayMusic(background_music, -1)
        Mix_FadeInMusic(background_music, -1, 8000)

    def process(self, world, components):
        if self.ball.soundeffects.current:
            Mix_PlayChannel(-1, self.ball.soundeffects.current, 0)
            self.ball.soundeffects.current = None


class TextureRenderer(ext.TextureSpriteRenderSystem):
    def __init__(self, renderer):
        super(TextureRenderer, self).__init__(renderer)
        self.renderer = renderer

    def render(self, components):
        #save current renderer.color before clearing the screen.
        tmp = self.renderer.color
        self.renderer.color = TURQUOISE
        self.renderer.clear()
        self.renderer.color = tmp
        super(TextureRenderer, self).render(components)


class ScoreDisplaySystem(ext.Applicator):
    def __init__(self, sprite_factory, font_manager, posx, posy):
        super(ScoreDisplaySystem, self).__init__()
        self.componenttypes = (PlayerData,)
        self.fontmanager = font_manager
        self.factory = sprite_factory
        self.posx = posx
        self.posy = posy

    def process(self, world, componentsets):
        score1 = self.player1.playerdata.score
        score2 = self.player2.playerdata.score
        score_display = str(score1) + " : " + str(score2)
        font_surface = self.fontmanager.render(text=score_display, color=LIGHT_BLUE, size=44)
        self.score.sprite = self.factory.from_surface(tsurface=font_surface, free=True)
        self.score.sprite.position = int(self.posx - self.score.sprite.size[0] / 2), int(self.posy)


class MovementSystem(ext.Applicator):
    def __init__(self, minx, miny, maxx, maxy):
        super(MovementSystem, self).__init__()
        self.componenttypes = Velocity, ext.Sprite
        self.minx = minx
        self.miny = miny
        self.maxx = maxx
        self.maxy = maxy

    def process(self, world, componentsets):
        for velocity, sprite in componentsets:
            swidth, sheight = sprite.size
            sprite.x += velocity.x
            sprite.y += velocity.y

            sprite.x = max(self.minx, sprite.x)
            sprite.y = max(self.miny, sprite.y)

            pmaxx = sprite.x + swidth
            pmaxy = sprite.y + sheight
            if pmaxx > self.maxx:
                sprite.x = self.maxx - swidth
            if pmaxy > self.maxy:
                sprite.y = self.maxy - sheight


class CollisionSystem(ext.Applicator):
    def __init__(self, minx, miny, maxx, maxy):
        super(CollisionSystem, self).__init__()
        self.componenttypes = Velocity, ext.Sprite, PlayerData
        self.ball = None
        self.minx = minx
        self.miny = miny
        self.maxx = maxx
        self.maxy = maxy

    def _overlap(self, item):
        pos, sprite, data = item
        if sprite == self.ball.sprite:
            return False

        left, top, right, bottom = sprite.area
        bleft, btop, bright, bbottom = self.ball.sprite.area

        return (bleft < right and bright > left and
                btop < bottom and bbottom > top)

    def process(self, world, componentsets):
        collitems = [comp for comp in componentsets if self._overlap(comp)]
        if collitems:
            self.ball.velocity.x = -self.ball.velocity.x
            sprite = collitems[0][1]
            ballcentery = self.ball.sprite.y + self.ball.sprite.size[1] // 2
            halfheight = sprite.size[1] // 2
            stepsize = halfheight // 10
            degrees = 0.7
            paddlecentery = sprite.y + halfheight
            if ballcentery < paddlecentery:
                factor = (paddlecentery - ballcentery) // stepsize
                self.ball.velocity.y = -int(round(factor * degrees))
                self.ball.soundeffects.current = self.ball.soundeffects.impact
            elif ballcentery > paddlecentery:
                factor = (ballcentery - paddlecentery) // stepsize
                self.ball.velocity.y = int(round(factor * degrees))
                self.ball.soundeffects.current = self.ball.soundeffects.impact
            else:
                self.ball.velocity.y = - self.ball.velocity.y
                self.ball.soundeffects.current = self.ball.soundeffects.impact

        if (self.ball.sprite.y <= self.miny or
                self.ball.sprite.y + self.ball.sprite.size[1] >= self.maxy):
            self.ball.velocity.y = - self.ball.velocity.y
            self.ball.soundeffects.current = self.ball.soundeffects.impact

        if self.ball.sprite.x <= self.minx:
            self.player2.playerdata.score += 1
            self.ball.sprite.x = int(screen_width  / 2)
            self.ball.velocity.x = - self.ball.velocity.x
            self.ball.velocity.y = randint(0,10)
            self.ball.soundeffects.current = self.ball.soundeffects.laser

        if self.ball.sprite.x + self.ball.sprite.size[0] >= screen_width:
            self.player1.playerdata.score += 1
            self.ball.sprite.x = int(screen_width / 2)
            self.ball.velocity.x = - self.ball.velocity.x
            self.ball.velocity.y = randint(0,10)
            self.ball.soundeffects.current = self.ball.soundeffects.coin


class TrackingAISystem(ext.Applicator):
    def __init__(self, miny, maxy):
        super(TrackingAISystem, self).__init__()
        self.componenttypes = PlayerData, Velocity, ext.Sprite
        self.miny = miny
        self.maxy = maxy
        self.ball = None

    def process(self, world, componentsets):
        for pdata, vel, sprite in componentsets:
            if not pdata.ai:
                continue

            centery = sprite.y + sprite.size[1] // 2
            if self.ball.velocity.x < 0:
                # ball is moving away from the AI
                if centery < self.maxy // 2:
                    vel.y = 3
                elif centery > self.maxy // 2:
                    vel.y = -3
                else:
                    vel.y = 0
            else:
                bcentery = self.ball.sprite.y + self.ball.sprite.size[1] // 2
                if bcentery < centery:
                    vel.y = -3
                elif bcentery > centery:
                    vel.y = 3
                else:
                    vel.y = 0


class Velocity(object):
    def __init__(self):
        super(Velocity, self).__init__()
        self.x = 0
        self.y = 0


class PlayerData(object):
    def __init__(self):
        super(PlayerData, self).__init__()
        self.ai = False
        self.score = 0


class SoundEffects(object):
    def __init__(self):
        super(SoundEffects, self).__init__()
        file = RESOURCES.get_path("laser.ogg")
        self.laser = Mix_LoadWAV(byteify(file, "utf-8"))
        file = RESOURCES.get_path("impact.ogg")
        self.impact = Mix_LoadWAV(byteify(file, "utf-8"))
        file = RESOURCES.get_path("coin.ogg")
        self.coin = Mix_LoadWAV(byteify(file, "utf-8"))
        self.current = None


class Ball(ext.Entity):
    def __init__(self, world, sprite, posx=0, posy=0):
        self.sprite = sprite
        self.sprite.position = posx, posy
        self.velocity = Velocity()
        self.soundeffects = SoundEffects()


class Player(ext.Entity):
    def __init__(self, world, sprite, posx=0, posy=0, ai=False, score=0):
        self.sprite = sprite
        self.sprite.position = posx, posy
        self.velocity = Velocity()
        self.playerdata = PlayerData()
        self.playerdata.ai = ai


class StaticSprite(ext.Entity):
    def __init__(self, world, sprite=None):
        self.sprite = sprite


def run():
    window = ext.Window(title="Pong", size=(screen_width, screen_height),
                        position=None, flags=SDL_WINDOW_SHOWN)
    window.show()
    renderer = ext.Renderer(target=window, flags=render.SDL_RENDERER_PRESENTVSYNC)
    #SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, b'best')
    SDL_RenderSetLogicalSize(renderer.renderer, screen_width, screen_height)
    target_frame_ms = (1000.0 / TARGETFPS) + 0.0000001

    # Create a world. The input class should become a System eventually.
    world = ext.World()
    controller = Input()
    # A Sprite "Factory" for creating sprites.
    factory = ext.SpriteFactory(sprite_type=ext.TEXTURE, renderer=renderer)
    fontmanager = ext.FontManager(font_path=RESOURCES.get_path("NotoSansJP.otf"), size=16)

    # Create Systems, and add them to the world.
    spriterenderer = TextureRenderer(renderer)
    movement = MovementSystem(minx=0, miny=0, maxx=screen_width, maxy=screen_height)
    collision = CollisionSystem(minx=0, miny=0, maxx=screen_width, maxy=screen_height)
    aisystem = TrackingAISystem(0, screen_height)
    scoresystem = ScoreDisplaySystem(sprite_factory=factory, font_manager=fontmanager,
                                     posx=(screen_width / 2), posy=5)
    audiosystem = AudioSystem(frequency=44100, channels=2, buffer_bytes=4096)

    world.add_system(spriterenderer)
    world.add_system(movement)
    world.add_system(collision)
    world.add_system(aisystem)
    world.add_system(scoresystem)
    world.add_system(audiosystem)

    # These are components of the entities:
    p1_sprite = factory.from_color(WHITE, size=(20, 100))
    p2_sprite = factory.from_color(WHITE, size=(20, 100))
    ball_sprite = factory.from_image(fname=RESOURCES.get_path("pongball.png"))
    background_sprite = factory.from_image(fname=RESOURCES.get_path("background.png"))
    background_sprite.depth = -1
    score_sprite = factory.from_color(WHITE, size=(10, 10))

    # These are entities:
    ball = Ball(world, ball_sprite, int(screen_width / 2 - 10), int(screen_height / 2 - 10))
    ball.velocity.x = -5
    player1 = Player(world=world, sprite=p1_sprite, posx=0, posy=230)
    player2 = Player(world=world, sprite=p2_sprite, posx=screen_width - p1_sprite.size[0], posy=230, ai=True)
    score = StaticSprite(world=world)
    background = StaticSprite(world=world, sprite=background_sprite)

    # Add the Entities to the appropriate systems
    collision.ball = ball
    collision.player1 = player1
    collision.player2 = player2
    aisystem.ball = ball
    scoresystem.player1 = player1
    scoresystem.player2 = player2
    scoresystem.score = score
    audiosystem.ball = ball


    running = True
    while running:
        start_time = SDL_GetTicks()
        new_events = ext.get_events()
        for event in new_events:
            if event.type == SDL_QUIT:
                running = False
        inputs = controller.update(new_events)
        if inputs["back"] == 1:
            print("Back/esc pressed. Quitting.")
            running = False

        if inputs["up"]:
            player1.velocity.y = -3
        elif inputs["down"]:
            player1.velocity.y = 3
        else:
            player1.velocity.y = 0


        world.process()

        #Frame rate limiter
        current_time = SDL_GetTicks()
        sleep_time = int(start_time + target_frame_ms - current_time)
        if sleep_time > 0:
            pass
            SDL_Delay(sleep_time)
        fps = int(1000 / (timer.SDL_GetTicks() - start_time))

ext.quit()

if __name__ == "__main__":
    sys.exit(run())